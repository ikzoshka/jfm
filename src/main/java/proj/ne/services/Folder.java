
package proj.ne.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.comparator.NameFileComparator;
import org.apache.commons.io.comparator.SizeFileComparator;

import proj.ne.servlets.Controller;


public class Folder
{

	private boolean isNotInContext;

	String path;

	String url;

	private File myFile;

	File[] children;

	private FileWrapper[] wrappers;

	private Map<String,File> nameToFile;

	private List<FileWrapper> wrappersList;

	private List parents;

	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

	private boolean calcRecursiveFolderSize = false;


	public static final int SORT_NAME_UP = 1;
	public static final int SORT_NAME_DOWN = 2;
	public static final int SORT_DATE_UP = 3;
	public static final int SORT_DATE_DOWN = 4;
	public static final int SORT_SIZE_UP = 5;
	public static final int SORT_SIZE_DOWN = 6;

	public boolean isCalcRecursiveFolderSize()
	{
		return calcRecursiveFolderSize;
	}

	public List getParents()
	{
		return parents;
	}

	private Folder()
	{
		// NOP
	}

	public Folder(File f, String path, String url)
			throws IOException
	{
		myFile = f;
		this.path = path;
		this.url = url;

		if (!myFile.exists())
		{
			throw new IOException(f.getPath() + " does not exist.");
		}
	}
	private String ftpToURL(String[] selectedIDs, String target)

	{
		URL url = null;
		String s = "";

		try
		{
			url = new URL("ftp://" + target);
		}
		catch (MalformedURLException e)
		{
			return "Malformed URL";
		}

		ArrayList l = new ArrayList();

		for (int i = 0; i < selectedIDs.length; i++)
		{
			File f = checkAndGet(selectedIDs[i]);

			if (null == f)
			{

			}

			l.add(f);
		}

		if (selectedIDs.length > 0)
		{
			s = Uploader.upload(url, l);
		}

		return s;
	}
	public List<FileWrapper> getFiles()
	{
		return wrappersList;
	}

	public void load()
	{

		children = myFile.listFiles();

		if (children == null)
		{
			return; // Windows special folders
		}

		wrappers = new FileWrapper[children.length];

		nameToFile = new HashMap<String,File>(children.length);

		for (int i = 0; i < children.length; i++)
		{
			String name = children[i].getName();

			wrappers[i] = new FileWrapper(this, i);

			nameToFile.put(name, children[i]);
		}

		wrappersList = Arrays.asList(wrappers);

		String[] pp = path.split("/");

		if ("/".equals(path))
		{
			pp = new String[1];
		}

		pp[0] = "/";

		HyperRefer[] parentLinks = new HyperRefer[pp.length];
		String s;
		int p = 0;
		for (int i = 0; i < pp.length - 1; i++)
		{
			s = path.substring(0, 1 + path.indexOf("/", p));
			p = s.length();
			parentLinks[i] = new HyperRefer(pp[i], s);
		}
		parentLinks[pp.length - 1] = new HyperRefer(pp[pp.length - 1], null);

		parents = Arrays.asList(parentLinks);

		sort(SORT_NAME_UP);
	}

	private boolean checkFileName(String name)
	{
		if (name.indexOf("..") > -1)
		{
			return false;
		}
		return true;
	}

	private String rename(String[] selectedIDs, String target)

	{
		if (selectedIDs.length > 1)
		{
			return "More than 1 file selected";
		}

		if (!checkFileName(target))
		{
			return "Illegal target name";
		}

		File f = checkAndGet(selectedIDs[0]);

		if (null == f)
		{

		}

		File f1 = new File(f.getParent(), target);

		if (f1.exists())
		{
			return target + " allready exists";
		}

		if (!f.renameTo(f1))
		{
			return "failed to rename " + f.getName();
		}

		return "";
	}

	private File getTargetFile(String target) throws IOException
	{
		File f = null;

		if (target.startsWith(File.separator))
		{
			f = new File(target);
		}
		else
		{
			f = new File(myFile, target);
		}

		f = f.getCanonicalFile();

		return f;
	}

	private File checkAndGet(String id)
	{
		String s = null;
		try
		{
			s = URLDecoder.decode(id, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			// NOP
		}

		String s1 = s.substring(0, s.lastIndexOf('.'));
		String s2 = s.substring(s.lastIndexOf('.') + 1);

		File f = nameToFile.get(s1);

		if (null == f)
		{
			return null; // File not found
		}

		long l = f.lastModified();

		if (!(Long.toString(l).equals(s2)))
		{
			return null; // File modification changed
		}

		return f;

	}

	private void fileCopy(File source, File target) throws IOException
	{
		FileInputStream in = new FileInputStream(source);
		FileOutputStream out = new FileOutputStream(target);
		int c;

		try
		{
			while ((c = in.read()) != -1)
			{
				out.write(c);
			}
			target.setLastModified(source.lastModified());
		}
		finally
		{
			out.close();
			in.close();
		}
	}

	public void sum()
	{
		calcRecursiveFolderSize = true;
	}

	public void sort(int mode)
	{
		Comparator<File> c = null;

		switch (mode)
		{
			case SORT_NAME_UP:
				c = NameFileComparator.NAME_COMPARATOR;
				break;
			case SORT_NAME_DOWN:
				c = NameFileComparator.NAME_REVERSE;
				break;
			case SORT_SIZE_UP:
				c = SizeFileComparator.SIZE_COMPARATOR;
				break;
			case SORT_SIZE_DOWN:
				c = SizeFileComparator.SIZE_REVERSE;
				break;
			case SORT_DATE_UP:
				c = LastModifiedFileComparator.LASTMODIFIED_COMPARATOR;
				break;
			case SORT_DATE_DOWN:
				c = LastModifiedFileComparator.LASTMODIFIED_REVERSE;
				break;
		}

		Arrays.sort(children, c);
	}

	public String copyOrMove(boolean move, String[] selectedIDs, String target)
			throws IOException
	{
		if ((selectedIDs == null) || (selectedIDs.length == 0))
		{
			return "No file selected";
		}

		File f1 = getTargetFile(target);

		if ((null == f1)
				|| (myFile.getCanonicalFile().equals(f1.getCanonicalFile())))
		{
			return "illegal target file";
		}

		if ((!f1.isDirectory()) && (selectedIDs.length > 1))
		{
			return "target is not a directory";
		}

		StringBuffer sb = new StringBuffer();

		File fx = null;

		for (int i = 0; i < selectedIDs.length; i++)
		{
			File f = checkAndGet(selectedIDs[i]);

			if (null == f)
			{

			}

			if (!f1.isDirectory())
			{
				fx = f1;
			}
			else
			{
				fx = new File(f1, f.getName());
			}

			if (move)
			{
				if (f.isDirectory())
				{
					FileUtils.moveDirectoryToDirectory(f, fx, true);
				}
				else {
					if (!f.renameTo(fx))
					{
						sb.append(f.getName()).append(" ");
					}
				}
			}
			else
			{
				try
				{
					if (f.isDirectory())
					{
						FileUtils.copyDirectoryToDirectory(f, fx);
					}
					else
					{
						FileUtils.copyFile(f, fx, true);
					}
				}
				catch (IOException e)
				{
					sb.append(f.getName()).append(" ");
				}
			}
		}

		String s = sb.toString();

		if (!"".equals(s))
		{
			String op = move ? "move" : "copy";
			return "failed to " + op + " " + s + " to " + f1.toString();
		}

		return "";
	}

	public String delete(String[] selectedIDs)

	{
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < selectedIDs.length; i++)
		{
			File f = checkAndGet(selectedIDs[i]);

			if (null == f)
			{

			}

			if (!f.delete())
			{
				sb.append(f.getName());
			}
		}

		String s = sb.toString();

		if (!"".equals(s))
		{
			return "failed to delete " + s;
		}

		return "";
	}

	public String download(String[] selectedIDs, String target)
			throws IOException {


		for (int i = 0; i < selectedIDs.length; i++) {
			File f = checkAndGet(selectedIDs[i]);
			String myfile = f.getName();
			URL website = new URL("file:///" + f.getAbsolutePath());
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			FileOutputStream fos = new FileOutputStream("C:\\Users\\Egor\\Downloads\\"+f.getName());
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

		}
		return "";
	}
//

	public String mkdir(String target) throws IOException
	{
		File f = getTargetFile(target);

		if (!f.mkdir())
		{
			return "could not mkdir " + target;
		}

		return "";
	}
 public String newfile (String target) throws IOException {
	 File f = getTargetFile(target);

	 if (!f.createNewFile())
	 {
		 return "could not create file " + target;
	 }

	 return "";
 }
	public String getURL(String url)
	{
		URL remote;
		try
		{
			remote = new URL(url);
		}
		catch (MalformedURLException e1)
		{
			return url + " is not a valid URL";
		}
		String s = remote.getFile();
		int p = s.lastIndexOf('/');
		if (p > -1)
		{
			s = s.substring(p);
		}
		File f = new File(myFile, s);

		try
		{
			FileUtils.copyURLToFile(remote, f, 1000, 1000);
		}
		catch (IOException e)
		{
			return "could not get " + remote.toString();
		}

		return "";
	}




	private String copyOrCutClipboard(String[] selectedIDs, int cutOrCopy, HttpSession session)
			throws IOException
	{
		File[] selectedfiles = new File[selectedIDs.length];

		for (int i = 0; i < selectedIDs.length; i++)
		{
			File f = checkAndGet(selectedIDs[i]);

			if (null == f)
			{

			}

			selectedfiles[i] = f;
		}

		CopyPasteContent clipBoardContent = new CopyPasteContent(cutOrCopy, selectedfiles);
		session.setAttribute("clipBoardContent", clipBoardContent);

		return "";
	}

	private String pasteClipboard(HttpSession session)
			throws  IOException
	{
		CopyPasteContent clipBoardContent = (CopyPasteContent)session.getAttribute("clipBoardContent");
		if (clipBoardContent == null)
		{
			return "nothing in clipboard";
		}

		for (int i = 0; i < clipBoardContent.selectedfiles.length; i++)
		{
			File f = clipBoardContent.selectedfiles[i];
			File f1 = f.getParentFile();
			
			if (myFile.getCanonicalFile().equals(f1.getCanonicalFile()))
			{
				return "same folder";
			}
		}
		
		for (int i = 0; i < clipBoardContent.selectedfiles.length; i++)
		{
			File f = clipBoardContent.selectedfiles[i];

			if (clipBoardContent.contentType == CopyPasteContent.COPY_CONTENT)
			{
				if (f.isDirectory())
				{
				 	FileUtils.copyDirectoryToDirectory(f, myFile);
				}
				else
				{
					FileUtils.copyFileToDirectory(f, myFile, true);
				}
			}
			if (clipBoardContent.contentType == CopyPasteContent.CUT_CONTENT)
			{
				if (f.isDirectory())
				{
				 	FileUtils.moveDirectoryToDirectory(f, myFile, false);
				}
				else
				{
					FileUtils.moveFileToDirectory(f, myFile, false);
				}
			}
			if (clipBoardContent.contentType == CopyPasteContent.CUT_CONTENT)
			{
				session.removeAttribute("clipBoardContent");
			}
		}

		return "";
	}



	public String action(int action, OutputStream out, String[] selectedIDs,
			String target, HttpSession session) throws IOException
	{
		String res = null;

		switch (action)
		{
			case Controller.RENAME_ACTION:
				res = rename(selectedIDs, target);
				break;
			case Controller.COPY_ACTION:
				res = copyOrMove(false, selectedIDs, target);
				break;
			case Controller.MOVE_ACTION:
				res = copyOrMove(true, selectedIDs, target);
				break;
			case Controller.CHMOD_ACTION:
				res = chmod(selectedIDs, target);
				break;
			case Controller.DELETE_ACTION:
				res = delete(selectedIDs);
				break;
			case Controller.DOWNLOAD_ACTION:
				res =download(selectedIDs,target);
				break;
//			case Controller.DELETE_RECURSIVE_ACTION:
//				res = deleteRecursive(selectedIDs, target);
//				break;
			case Controller.MKDIR_ACTION:
				res = mkdir(target);
				break;
			case Controller.NEWFILE_ACTION:
				res = newfile(target);
				break;
			case Controller.GETURL_ACTION:
				res = getURL(target);
				break;
			case Controller.CLIPBOARD_COPY_ACTION:
				res = copyOrCutClipboard(selectedIDs, CopyPasteContent.COPY_CONTENT, session);
				break;
			case Controller.CLIPBOARD_CUT_ACTION:
				res = copyOrCutClipboard(selectedIDs, CopyPasteContent.CUT_CONTENT, session);
				break;
			case Controller.CLIPBOARD_PASTE_ACTION:
				res = pasteClipboard(session);
				break;
		}

		if ("".equals(res)) // no error, action succeded.
		{
			load();
		}

		return res;
	}

	public void upload(FileItem item, boolean unzip) throws Exception
	{
		String name = item.getName();

		name = name.replaceAll("\\\\", "/");
		int p = name.lastIndexOf('/');
		if (p > -1)
		{
			name = name.substring(p);
		}
		if (unzip)
		{
			InputStream is = item.getInputStream();

		}
		else
		{
			File f = new File(myFile, name);
			item.write(f);
		}
	}

	private String chmod(String[] selectedIDs, String target)
			throws  IOException
	{
		for (int i = 0; i < selectedIDs.length; i++)
		{
			File f = checkAndGet(selectedIDs[i]);

			if (null == f)
			{

			}

			chmod(f, target);
		}
		return "";
	}

	private void chmod(File file, String value) throws IOException
	{
		Path p = file.toPath();

		FileSystem fileSystem = FileSystems.getDefault();
		Set<String> fileSystemViews = fileSystem.supportedFileAttributeViews();
		
		if (fileSystemViews.contains("posix"))
		{
			Set<PosixFilePermission> posixFilePermissions = PosixFilePermissions
					.fromString(value);

			if (posixFilePermissions == null)
			{
				throw new IllegalArgumentException(value);
			}

			Files.getFileAttributeView(p, PosixFileAttributeView.class)
					.setPermissions(posixFilePermissions);
		}
		else if (fileSystemViews.contains("dos"))
		{
		}
	}

}