package proj.ne.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class textEditor {
    private static textEditor instance = new textEditor();

    private textEditor(){}

    public static textEditor getInstance(){
        return instance;
    }

    public void editText(String text, String path){
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(path))){
            writer.write(text);
            writer.flush();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

}
