package proj.ne.servlets;



import java.io.*;
import java.security.Principal;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;

import proj.ne.services.Folder;


public class Controller extends HttpServlet {

	private static final String PATH_URL_SERVLET = "/path";

	private static final String CTX_DOWNLOAD_SERVLET = "/dlx";

	private static final String FILE_DOWNLOAD_SERVLET = "/dlf";


	public static final int NOP_ACTION = 0;
	public static final int RENAME_ACTION = 1;
	public static final int COPY_ACTION = 2;
	public static final int MOVE_ACTION = 3;
	public static final int DELETE_ACTION = 4;
	public static final int MKDIR_ACTION = 5;
	public static final int NEWFILE_ACTION = 6;
	public static final int DOWNLOAD_ACTION = 7;
	public static final int GETURL_ACTION = 8;
	public static final int FTPUP_ACTION = 9;
	public static final int CHMOD_ACTION = 10;
	public static final int CLIPBOARD_COPY_ACTION = 11;
	public static final int CLIPBOARD_CUT_ACTION = 12;
	public static final int CLIPBOARD_PASTE_ACTION = 13;



	private Properties dirmapping = null;

	private File tempDir = null;

	private String filebase = null;

	public void init() throws ServletException {
		tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");

		filebase = getServletContext().getInitParameter("filebase");



	}
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		doPost(request, response);
	}

@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String self = null;
		String contextPath = null;
		String pathInfo = null;
		Folder folder = null;
		String queryString = null;

		try {
			contextPath = request.getContextPath();
			String servletPath = request.getServletPath();
			String method = request.getMethod();
			boolean formPosted = "POST".equals(method);

			pathInfo = request.getPathInfo();

			if (null == pathInfo) {

				PrintWriter writer = response.getWriter();
				writer.print(contextPath + servletPath + " is alive.");

				return;
			}

			File f = new File(filebase, pathInfo);

			if (!f.exists()) {

				PrintWriter writer = response.getWriter();
				writer.print(contextPath + pathInfo + " does not exist.");

				return;
			}

			if (!f.isDirectory()) {
				doDownload(request, response, f);
				return;
			}

			if (!pathInfo.endsWith("/")) {
				response.sendRedirect(request.getRequestURL() + "/");
				return;
			}

			queryString = request.getQueryString();

			String pathTranslated = request.getPathTranslated();
			String requestURI = request.getRequestURI();
			String requestURL = request.getRequestURL().toString();

			self = contextPath + servletPath;

			String fileURL = requestURI.replaceFirst(contextPath, "");
			fileURL = fileURL.replaceFirst(servletPath, "");

			folder = new Folder(f, pathInfo, fileURL);

			folder.load();

			String actionresult = "";

			if (FileUpload.isMultipartContent(request)) {
				try {
					actionresult = handleUpload(request, folder);
					folder.load();
				} catch (Exception e) {
					throw new ServletException(e.getMessage(), e);
				}
			} else if (formPosted || null != queryString) {
				try {
					actionresult = handleQuery(request, response, folder);
				} catch (Exception e) {
					actionresult = e.getMessage();
				}
				if (null == actionresult) {
					return;
				}
			}

			request.setAttribute("actionresult", actionresult);
		} catch (SecurityException e) {
			request.setAttribute("actionresult", e.getClass().getName() + " " + e.getMessage());
			request.setAttribute("fatalerror", new Boolean(true));

		}

		String s = request.getRemoteUser();

		Principal principal = request.getUserPrincipal();

		if (principal != null) {
			request.setAttribute("principal", principal.getName());
		}

		request.setAttribute("self", self);

		request.setAttribute("date", s);

		request.setAttribute("javaversion", System.getProperty("java.version"));

		request.setAttribute("serverInfo", getServletContext().getServerInfo());

		request.setAttribute("jfmhome", "https://java.net/projects/jfm");

		request.setAttribute("url", contextPath);

		request.setAttribute("path", pathInfo);

		request.setAttribute("folder", folder);

		String forward = "/WEB-INF/fm.jsp";

		if (queryString != null) {

		}

		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forward);

		requestDispatcher.forward(request, response);
	}


	private String handleQuery(HttpServletRequest request, HttpServletResponse response, Folder folder)
			throws IOException {
		String rc = "";

		HttpSession session = request.getSession();

		String target = null;
		int action = NOP_ACTION;
		String[] selectedfiles = request.getParameterValues("index");

		String logout = request.getParameter("logout");
		if ("t".equals(logout)) {
			request.getSession().invalidate();
			return "";
		}

		String sum = request.getParameter("sum");
		if ("t".equals(sum)) {
			folder.sum();
			return "";
		}
		String sort = request.getParameter("sort");
		if ("nd".equals(sort)) {
			folder.sort(Folder.SORT_NAME_DOWN);
			return "";
		} else if ("nu".equals(sort)) {
			folder.sort(Folder.SORT_NAME_UP);
			return "";
		} else if ("su".equals(sort)) {
			folder.sort(Folder.SORT_SIZE_UP);
			return "";
		} else if ("sd".equals(sort)) {
			folder.sort(Folder.SORT_SIZE_DOWN);
			return "";
		} else if ("du".equals(sort)) {
			folder.sort(Folder.SORT_DATE_UP);
			return "";
		} else if ("dd".equals(sort)) {
			folder.sort(Folder.SORT_DATE_DOWN);
			return "";
		}

		OutputStream out = null;

		String command = request.getParameter("command");
		if ("Mkdir".equals(command)) {
			target = request.getParameter("newdir");
			action = MKDIR_ACTION;
		}
		if ("Createfile".equals(command)) {
			target = request.getParameter("newfile");
			action = NEWFILE_ACTION;

		} else if ("Delete".equals(command)) {
			action = DELETE_ACTION;
		} else if ("Rename to".equals(command)) {
			target = request.getParameter("renameto");
			action = RENAME_ACTION;
		} else if ("download".equals(command)) {
			action = DOWNLOAD_ACTION;
		} else if ("Copy to".equals(command)) {
			target = request.getParameter("copyto");
			action = COPY_ACTION;
		} else if ("Move to".equals(command)) {
			target = request.getParameter("moveto");
			action = MOVE_ACTION;
		} else if ("Chmod to".equals(command)) {
			target = request.getParameter("chmodto");
			action = CHMOD_ACTION;
		} else if ("FtpUpload to".equals(command)) {
			target = request.getParameter("ftpto");
			action = FTPUP_ACTION;
		} else if ("cut".equals(command)) {
			action = CLIPBOARD_CUT_ACTION;
		} else if ("copy".equals(command)) {
			action = CLIPBOARD_COPY_ACTION;
		} else if ("paste".equals(command)) {
			action = CLIPBOARD_PASTE_ACTION;

		}


		if (NOP_ACTION == action) {
			return "";
		}

		try {
			rc = folder.action(action, out, selectedfiles, target, session);
		} catch (SecurityException e) {
			rc = "SecurityException: " + e.getMessage();
			return rc;
		}

		folder.load();

		return rc;
	}

	private String handleUpload(HttpServletRequest request, Folder folder) throws Exception {
		DiskFileUpload upload = new DiskFileUpload();
		upload.setRepositoryPath(tempDir.toString());
		System.out.println(upload.getSizeMax());

		List items = upload.parseRequest(request);

		Iterator itr = items.iterator();

		boolean unzip = false;

		while (itr.hasNext()) {
			FileItem item = (FileItem) itr.next();

			if (item.isFormField()) {
				String name = item.getFieldName();
				String value = item.getString();
				if ("command".equals(name) && "unzip".equals(value)) {
					unzip = true;
				}
			} else {
				String name = item.getFieldName();
				unzip = "unzip".equals(name);

				if (!"".equals(item.getName())) {
					folder.upload(item, unzip);
				}
					}
		}
		return "";
	}



public void doDownload(HttpServletRequest request, HttpServletResponse response, File f) throws IOException {

	String name = f.getName();

	String mimeType = getServletContext().getMimeType(name);

	response.setContentType(mimeType);
	response.setContentType(mimeType);



	response.setHeader("Content-Disposition", "inline; filename=\"" + name + "\"");

	OutputStream out = response.getOutputStream();



	FileInputStream in = new FileInputStream(f);


	byte[] buf = new byte[512];
	int l;

	try {
		while ((l = in.read(buf)) > 0) {
			out.write(buf, 0, l);
		}
	} catch (IOException e) {
		throw e;
	} finally {
		in.close();
	}

}
}
