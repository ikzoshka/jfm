package proj.ne.servlets;

import proj.ne.services.notetextEditor;


        import javax.servlet.ServletException;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
        import java.io.IOException;


    public class NoteEditor extends HttpServlet {
        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String text = req.getParameter("newText");
            String path =  (String)req.getSession().getAttribute("filebase") +  req.getSession().getAttribute("path".replaceAll("/", "\\")) + req.getParameter("fileName");


            notetextEditor.getInstance().editText(text, path);
            getServletContext().getRequestDispatcher("/p/").forward(req, resp);
        }
    }