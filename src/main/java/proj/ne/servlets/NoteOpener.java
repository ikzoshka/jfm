package proj.ne.servlets;

import proj.ne.services.noteTextReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NoteOpener extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fileName = req.getParameter("name");
        String path =  (String) req.getSession().getAttribute("filebase") +  req.getSession().getAttribute("path".replaceAll("/", "\\")) +  fileName;
        String text = noteTextReader.getInstance().getText(path);
        req.setAttribute("text", text);
        req.setAttribute("fileName", fileName);
        getServletContext().getRequestDispatcher("/WEB-INF/editor.jsp").forward(req, resp);
    }
}
