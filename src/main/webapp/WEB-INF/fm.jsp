
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
  <title>File Manager </title>
  <meta http-equiv="Content-Type"
 content="text/html; charset=UTF-8">

  <link rel="stylesheet" type="text/css" href="${url}/styles/main.css" />
  <link rel="stylesheet" type="text/css" href="${url}/styles/style.css" />

  <link rel="shortcut icon" href="${url}/img/application-small-blue.png" type="image/gif" />

</head>




<body>

<ul>
<li><a href="?sessionLocale=en"><fmt:message key="label.lang.en" /><img src="${url}/img/England.png" title="download ${file.name}" width="19" height="19"  alt="DEL" border="0"></a></li>
<li><a href="?sessionLocale=it"><fmt:message key="label.lang.it" /><img src="${url}/img/Italy.png" title="download ${file.name}" width="19" height="19"  alt="DEL" border="0"></a></li>
<p>
</ul>


<span style="color: rgb(189,224,191);">${actionresult}</span>

<c:if test="${!fatalerror}"> 

<form name="files" action="${self}${path}" method="post">

<table border="1">
<tr>
<td class="title" style="border-right-width: 0;">

  &nbsp;

<%--  <a href="${self}${path}"><img src="${url}/img/arrow-circle-225-left.png" title="reload folder" width="24" height="24" alt="RELOAD" border="0"></a>--%>

</td>
<td class="title" style="border-left-width: 0; text-align: right;"
  <c:set var="parentlink" value="" scope="request"/>

  <c:forEach var="parent" items="${folder.parents}" varStatus="status">

    <c:choose>
      <c:when test="${parent.isActive}">
        <a href="${self}${parent.link}">${parent.display}</a>
      </c:when>
      <c:otherwise>
        ${parent.display}
      </c:otherwise>
    </c:choose>

    <c:if test="${!status.last}">

      <c:set var="parentlink" value="${self}${parent.link}" scope="request"/>
    </c:if>

  </c:forEach>

  &nbsp;

  <c:if test="${parentlink != ''}">
    <a href="${parentlink}"><img src="${url}/img/goback.png" title="back" width="24" height="24" alt="UP" border="0"></a>
  </c:if></td>
</tr>

</table>

<table class="files">
  <thead>

  <tr>
    <td class="header-center" style="width: 8%;">
      <script>
        function doChkAll(oChkBox) {
          var bChecked = oChkBox.checked;
          var docFrmChk = document.forms['files'].index;
          for (var i = 0; i < docFrmChk.length; i++) {
            docFrmChk[i].checked = bChecked;
          }
        }
      </script>
      <small>
        <fmt:message key="label.check-all" />
        <input type="checkbox" name="chkAll" onclick="doChkAll(this);">
      </small>
    </td>



<td class="header-center" style="width: 15%;"><small><fmt:message key="label.filename" /></small> &nbsp;
<a href="${self}${path}?sort=nu">
<img src="${url}/img/sortup.png" title="sort by name ascending" width="19" height="19" alt="SORTUP" border="0"></a>
&nbsp;

<a href="${self}${path}?sort=nd">

<img src="${url}/img/sortdown.png" title="sort by name descending" width="19" height="19" alt="SORTDN" border="0"></a>

</td>
<td class="header-center" style="width: 3em;"><small><fmt:message key="label.type" /></small></td>

<td class="header-center" style="width: 10%;"><small><fmt:message key="label.size" /></small>
&nbsp;
<a href="${self}${path}?sort=su">
<img src="${url}/img/sortup.png" title="sort by size ascending" width="19" height="19" alt="SORTUP" border="0"></a>

&nbsp;

<a href="${self}${path}?sort=sd">
<img src="${url}/img/sortdown.png" title="sort by size descending" width="19" height="19" alt="SORTDN" border="0"></a>


&nbsp;



</td>
<td class="header-center" style="width: 11%;"><small><fmt:message key="label.last-modification" /></small>&nbsp;

</td>

</tr>

</thead>

<tbody>

<c:forEach var="file" items="${folder.files}">

<tr>
<td class="row-right">
  <c:if test="${!file.isDirectory && file.type=='txt'}">
<%--    <input type="button" onclick="location.href='${pageContext.request.contextPath}/opi?name=${file.name}';" value="Edit" />--%>

    <input type="button" onclick="location.href='${pageContext.request.contextPath}/opi?name=${fn:replace(file, '\\', '/')}';" value="Edit" />
  </c:if>

<c:if test="${!file.isDirectory}">
<a href="${self}${path}?command=download&index=${file.id}"><img src="${url}/img/download.png" title="download ${file.name}" width="19" height="19" alt="DEL" border="0"></a>
</c:if>
<%--  <c:if test="${!file.isDirectory ||file.isDirectory} ">--%>
   <input type="button"  onclick="location.href='${pageContext.request.contextPath}/open?name=${file.name}';" value="Notes" />




<%--      <input type="button" onclick="location.href='${contextPath}/open?name=${file.name}';" value="Edit" />--%>
<%--      <input type="button" onclick="location.href='${pageContext.request.contextPath}/open?name=${file.name}';" value="Go to Google" />--%>


<small><input type="checkbox" name="index" value="${file.id}"></small></td>

<td class="row-center"><small><c:choose>
  <c:when test="${file.type=='f'}">
    <a href="${self}${file.path}"><img src="${url}/img/folder.png" title="folder" width="19" height="19" alt="DIR" border="0"></a>
    <a href="${self}${file.path}">${file.name}</a>
  </c:when> 
<c:when test="${file.type=='docx'}">
  <a href="${self}${file.path}"><img src="${url}/img/word.png" title="word" width="19" height="19" alt="FILE" border="0"></a>
  <a ${self}${file.path}>${file.name}</a>
  </c:when>
  <c:when test="${file.type=='pdf'}">
    <a href="${self}${file.path}"><img src="${url}/img/pdf.png" title="pdf" width="19" height="19" alt="DIR" border="0"></a>
    <a href="${self}${file.path}">${file.name}</a>
  </c:when>
    <c:when test="${file.type=='txt'}">
        <a href="${self}${file.path}"><img src="${url}/img/txt.png" title="txt" width="19" height="19"  alt="DIR" border="0"></a>
        <a href="${self}${file.path}">${file.name}</a>


    </c:when>
    <c:otherwise>
    <a href="${self}${file.path}"><img src="${url}/img/file.png" title="txt or other" width="16" height="16" alt="DIR" border="0"></a>
    <a${self}${file.path}>${file.name}</a>
  </c:otherwise>

</c:choose>  </small></td>
<td class="row-center">${file.type}</td>

<td class="row-center">${file.size} </td>


<td class="row-center">${file.lastModified}</td>


</tr>

</c:forEach>

</tbody>
</table>

  <table>
    <tbody>

    <tr>
      <td colspan="3" class="header-left">Ctrl+c,z,v</td>


<%--      <input type="button" value="<fmt:message key="submitKey" />" />--%>
      <td class="row-center"><button input type="submit" name="command" value=cut><fmt:message key="label.cut" /></button></td>
   <td class="row-center"><button input type="submit" name="command" style="font-size:small;" value=copy><fmt:message key="label.copy" /></button></td>
    <td class="row-center"><button input type="submit" name="command" value=paste><fmt:message key="label.paste" /></button></td>

    <c:if test="${not empty sessionScope.clipBoardContent}">
      <tr><td colspan="6" style="font-size:small;"><fmt:message key="label.action" /> ${sessionScope.clipBoardContent.kind} this ${sessionScope.clipBoardContent.fileCount} files.
      </td></tr>
      <tr><td colspan="6" style="font-size:small;"><fmt:message key="label.forget" /> ${sessionScope.clipBoardContent.files}</td></tr>
    </c:if>
    <tr><td colspan="6" class="header-left"><fmt:message key="label.Action-on-selected-Files" /></td></tr>


    <tr>
      <td class="row-right"><button input type="submit" name="command" value="Rename to"><fmt:message key="label.renameto" /></button></td>
      <td class="row-left"><input name="renameto" type="text"></td>



      <td class="row-right"> <button input type="submit" name="command" value=Delete><fmt:message key="label.delete" /></button></td>
      <td class="row-center"><button input type="submit" name="command" value=download> <fmt:message key="label.download" /></button></td>

      <td class="row-right">

        <form action="${self}${path}" method="post">

          <button input type="submit" name="command" value=Mkdir><fmt:message key="label.mkdir" /></button>
          <input name="newdir" type="text">

        </form>
      </td>
      <td class="row-right">

        <form action="${self}${path}" method="post">

          <button input type="submit" name="command" value=Createfile><fmt:message key="label.createfile" /></button>
          <input name="newfile" type="text">

        </form>
      </td>
    </tr>
<%--      <td class="row-left"> <input type="submit" name="command" value="Join" title="Append to first selected file other selected files"></td>--%>







    </tbody>
  </table>

</form>

<form action="${self}${path}"
 method="post" enctype="multipart/form-data">

<table border="1">
<tbody>
<tr>

<td colspan="5" class="title">
  <fmt:message key="label.File-upload-to-current-directory" />
</td>

</tr>

<tr>
  <td class="row-right"><fmt:message key="label.choosefile"></fmt:message></td> <td class="row-left"><input type="file" name="myimage"></td>

  <td class="row-right"></td>


</tr>

<tr>
  <td></td>
  <td class="row-left"><input type="submit" name="command" value="Upload"/></td>

  </td>

  </tr>

  </tbody>
  </table>

</c:if>


</body>
</html>